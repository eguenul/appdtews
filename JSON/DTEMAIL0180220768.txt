Resultado de Validacion de Envio de DTE
=======================================

Rut de Empresa Emisora   : 77454072-5
Rut que Realizo el Envio : 13968481-8
Identificador de Envio   :  180220768
Fecha de Recepcion       : 26/12/2022 20:03:01
Estado del Envio         : EPR - Envio Procesado

Estadisticas del Envio
======================
Tipo DTE  Informados  Rechazos  Reparos  Aceptados
--------------------------------------------------
   33              1         0        0          1

