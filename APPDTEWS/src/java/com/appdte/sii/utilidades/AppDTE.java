package com.appdte.sii.utilidades;

import appdtews.documento.DocumentoModel;
import appdtews.empresa.EmpresaModel;
import appdtews.movimientos.Movimiento;
import appdtews.movimientos.MovimientoModel;
import com.appdte.json.DescGlobalJson;
import com.appdte.models.DetalleDteModel;
import com.appdte.models.DteModel;
import com.appdte.models.ReferenciaModel;
import com.appdte.json.ReceptorJson;
import com.appdte.json.EmisorJson;
import com.appdte.json.IdDteJson;
import com.appdte.json.ReferenciaJson;
import com.appdte.json.DetalleDteJson;
import com.appdte.json.TotalesJson;
import com.appdte.json.DteJson;
import com.appdte.json.ImptoRetenJson;
import com.appdte.json.TrackID;
import com.appdte.models.ImptoReten;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;
import com.appdte.sii.cl.Semilla;
import com.appdte.sii.cl.Token;
import com.appdte.sii.cl.UploadSii;
/*
import appventas.movimientos.BlobDTE;

*/
import com.google.gson.Gson;

import java.util.List;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;


public class AppDTE {
    String environment;
    
            
            
    
    
    
    @SuppressWarnings("empty-statement")
    
    public String sendDTE(String stringDTE,String certificado,String clave,String rutEnvia, boolean blReferencia) throws TransformerException, ParserConfigurationException, SAXException, IOException, Exception{

        
        
        
        
        String login = certificado;
        
        
        
   ConfigAppDTE objconfig = new ConfigAppDTE();
        /* CARGO LOS PARAMETROS DE CONFIGURACION */
        /* ingreso el DTE en formato JSON */
  
             
   System.out.println("Reading JSON from a file");
   System.out.println("----------------------------");
  
   
  
    InputStream isjson = new ByteArrayInputStream(stringDTE.getBytes("ISO-8859-1")); 
    BufferedReader br1 = new BufferedReader(new InputStreamReader(isjson));
  
  
    Gson gson = new Gson(); 
    DteJson objdtejson = gson.fromJson(br1, DteJson.class);
 
        
     
     /* DATOS DEL EMISOR EN JSON */
     EmisorJson objemisor = objdtejson.getEmisor();
     
     
     
     
     
    /* DATOS DEL RECEPTOR EN JSON */
    ReceptorJson objreceptor = objdtejson.getReceptor();
    
    IdDteJson iddoc = objdtejson.getIddoc();
    
    TotalesJson totales = objdtejson.getTotales(); 
   
    List<ImptoRetenJson> array_impuestosjson = objdtejson.getTotales().getImptoreten();
    ArrayList<ImptoReten> array_impuestos = new ArrayList<>(); 
    
    
    if(array_impuestosjson!=null){
        
        for(ImptoRetenJson j: array_impuestosjson){
        
            ImptoReten objImpuesto = new ImptoReten();
            objImpuesto.setMontoimp(j.getMontoimp());
            objImpuesto.setTasaimp(j.getTasapimp());
            objImpuesto.setTipoimp(j.getTipoimp());
            array_impuestos.add(objImpuesto);
        }
    }
    
    
    
    
   /* inicializar el xml */        
    DteModel objdte = new DteModel();
    objdte.setRutrecepcaratula(objreceptor.getRutcaratula());
    ClassDteDao obj = new ClassDteDao();
    DetalleDteModel objdetalledte = new DetalleDteModel();
    /* DEFINO DATOS DEL EMISOR Y RECEPTOR 
   */
   
    
    
    objdte.setFchvenc(iddoc.getFchvenc());
    objdte.setRutemisor(objemisor.getRutemisor());
    objdte.setTipodte(iddoc.getTipodte());
    objdte.setNumdte(iddoc.getNumdte());
     
    /* VALIDAMOS CAF */
        /*
        if(objFuncionCAF.validaCAF(objconfig.getPathcaf(), objemisor.getRutemisor(),Integer.parseInt(iddoc.getTipoDTE()), Integer.parseInt(iddoc.getNumDTE()))==false){
         */
        /*
        return null;
         */
        /*  }*/
    
    
    objdte.setFechadte(iddoc.getFechaEmision());
    
    if(Integer.parseInt(iddoc.getTipodte())==52){
        objdte.setTipotraslado(iddoc.getTipotraslado());
    }
    
    
    if(Integer.parseInt(iddoc.getTipodte())==33 || Integer.parseInt(iddoc.getTipodte())==34 ){
        objdte.setFrmapago(iddoc.getFrmapago());
    }
    
    
    String[] arrayrutemisor = objdte.getRutemisor().split("-");
    
    String rutemisor = arrayrutemisor[0];
    String nombredte = "DTE"+rutemisor+"F"+iddoc.getNumdte()+"T"+iddoc.getTipodte();
        
    
    
    
    
    objdte.setFecharesol(objemisor.getFecharesol());
    objdte.setNumresol(objemisor.getNumresol());
    
    /* DATOS DEL EMISOR EN EL XML */
  
   objdte.setRsemisor(objemisor.getRsemisor());
   objdte.setGiroemisor(objemisor.getGiroemisor());
   objdte.setActecoemisor(objemisor.getActecoemisor());
  
    
   objdte.setCodigosii(objemisor.getCodsiisucur());
   objdte.setDiremisor(objemisor.getDiremisor());
   objdte.setCmnaemisor(objemisor.getCmnaemisor());
   objdte.setCiuemisor(objemisor.getCiuemisor());
    
    
    /* DATOS DEL RECEPTOR EN EL XML */
    objdte.setRutreceptor(objreceptor.getRutreceptor());
    objdte.setRsreceptor(objreceptor.getRsreceptor());
    objdte.setGiroreceptor(objreceptor.getGiroreceptor());
    objdte.setCmnareceptor(objreceptor.getCmnareceptor());
    objdte.setCiureceptor(objreceptor.getCiureceptor());
    objdte.setDirreceptor(objreceptor.getDirreceptor());
    objdte.setNumdte(iddoc.getNumdte());
    
    
    /* DEFINO EL TOTAL Y TASA DE IMPUESTO */
     objdte.setMontofecto(totales.getMontoafecto());
     objdte.setMontexento(totales.getMontoexento());
     objdte.setMontoiva(totales.getMontoiva());
    objdte.setTasaiva(totales.getTasaiva());
    objdte.setImptoreten(array_impuestos);
    objdte.setMontototal(totales.getMontototal());
      
    /* INICIALIZO EL OBJETO DE DOCUMENTO */
    obj.crearXml(objdte);
    /* agrego los impto retenidos */
 
    
      /* cargo los detalles */
     List<DetalleDteJson> detalle = objdtejson.getDetalleDteJson();
  
    
for (DetalleDteJson i :  detalle){     
 
   
    
    objdetalledte.setNrolinea(i.getNrolinea());
    objdetalledte.setTpocodigo(i.getTpocodigo());
    objdetalledte.setVlrcodigo(i.getVlrcodigo());
    objdetalledte.setNmbitem(i.getNmbitem());
    objdetalledte.setDscitem(i.getNmbitem());
    objdetalledte.setQtyitem(i.getQtyitem());
    objdetalledte.setPrcitem(i.getPrcitem());
    
    objdetalledte.setDescuentopct(i.getDescuentopct());
    objdetalledte.setDescuentomonto(i.getDescuentomonto());
    objdetalledte.setMontoitem(i.getMontoitem());
    objdetalledte.setIndexe(i.getIndexe());
    objdetalledte.setCodimpadic(i.getCodimpadic());
    obj.agregaDetalle(objdetalledte);
    
    }

/* ADJUNTO LOS DESCUENTOS GLOBALES   */

List<DescGlobalJson> arraydescuentos = objdtejson.getDescuentoglobal();

if(arraydescuentos!=null){ 
for (DescGlobalJson x :  arraydescuentos){
    
   obj.agregaDescuento(x);
   
   
}     
}      



  
   ReferenciaJson referencia = objdtejson.getReferencia();
   ReferenciaModel objReferenciaModel = new ReferenciaModel();
   objReferenciaModel.setRazonref(referencia.getRazonref());
   objReferenciaModel.setNumref(referencia.getNumref());
   
   objReferenciaModel.setFecharef(referencia.getFecharef());
   objReferenciaModel.setFolioref(referencia.getFolioref());
   objReferenciaModel.setCodref(referencia.getCodref());
    
   /*     objReferenciaModel.setTpoDocRef(referencia.getTpoDocRef()); */
       
objReferenciaModel.setTpoDocRef(referencia.getTpoDocRef());
    
obj.agregaRegerencia(objReferenciaModel,blReferencia);
    

obj.guardarDocumento(nombredte,objconfig.getPathdte());



/*
objTimbre.creaTimbre(objdte, auxDescripcion,rutemisor);
  
    
/* preparo el DTE para firmar */

/* preparo el DTE para firmar */

Timbre objTimbre = new Timbre();
 
objTimbre.creaTimbre(login,objconfig.getPathdte(),nombredte,objconfig.getPathdata(),objconfig.getPathcaf(),rutemisor);



SignDTE objFirma = new SignDTE();
objFirma.signDTE(objconfig.getPathdte(),nombredte,certificado,clave);
   
    /* ahora envuelvo el DTE en un sobre electrónico */
   
EnvioDTE objenvio = new EnvioDTE(this.environment, objreceptor.getRutcaratula());
objenvio.generaEnvio(objdte,nombredte,objconfig.getPathdte(),rutEnvia);

SignENVDTE objFirmaENV = new SignENVDTE();
objFirmaENV.signENVDTE(objconfig.getPathdte(),nombredte,certificado,clave);
    

 /* OBTENGO LA SEMILLA PARA AUTENTIFICARME AL SII   */ 
 
  Semilla objsemilla = new Semilla();

  
 
String valorsemilla =  objsemilla.getSeed(objconfig.getPathenvironment());
 

 Token objtoken = new Token(objconfig.getPathenvironment());
 String valortoken =  objtoken.getToken(valorsemilla,certificado,clave,nombredte);


UploadSii objupload = new UploadSii(objconfig.getPathenvironment());
 





TrackID objTrackID = objupload.uploadSii(valortoken,"","ENV"+ nombredte,objdte.getRutemisor(),rutEnvia);

   
   EmpresaModel objEmpresaModel = new EmpresaModel();  
   int empresaid = objEmpresaModel.getIdEmpresafromRut(objemisor.getRutemisor());
   DocumentoModel objDocumentoModel = new DocumentoModel();
   int idtipodoc = objDocumentoModel.getIddocumento2(Integer.parseInt(iddoc.getTipodte()));
   Movimiento objMovimiento = new Movimiento();
   TotalesJson objtotales = objdtejson.getTotales();
   objMovimiento.setFecha(iddoc.getFechaEmision());
   objMovimiento.setEmpresaid(empresaid);
   objMovimiento.setRutemisor(objemisor.getRutemisor());
   objMovimiento.setRutreceptor(objreceptor.getRutreceptor());
   objMovimiento.setRutenvia(rutEnvia);
   objMovimiento.setIdtipodoc(idtipodoc);
   objMovimiento.setTotalbruto(objtotales.getMontototal());
   objMovimiento.setNumdoc(Integer.parseInt(iddoc.getNumdte()));
   objMovimiento.setArchivonom(objTrackID.getFile());
   objMovimiento.setTrackid(Integer.parseInt(objTrackID.getTrackid()));
   MovimientoModel objMovimientoModel = new MovimientoModel();
   objMovimientoModel.addDTE(objMovimiento);
   
   Gson gson2 = new Gson(); 

return gson2.toJson(objTrackID);
 
 
 
 
}
   

}