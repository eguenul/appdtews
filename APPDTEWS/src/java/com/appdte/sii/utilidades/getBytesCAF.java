/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.appdte.sii.utilidades;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author esteban
 */
public class getBytesCAF {
 
 public byte[] getBytesCAF(String filePath){
     
     try {
         System.setProperty("file.encoding", "ISO-8859-1");
         byte[] bytes = Files.readAllBytes(Paths.get(filePath));
    
         String strDATA = new String(bytes, "ISO-8859-1");
         String strCleaned = strDATA.replaceAll("&#13;", "");
         
         byte[] bytes2 = strCleaned.getBytes("ISO-8859-1");
    
         return bytes2;
     } catch (IOException ex) {
         Logger.getLogger(getBytesCAF.class.getName()).log(Level.SEVERE, null, ex);
     }
     return null;
          
      
     
 }
    
    
}
