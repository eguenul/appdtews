/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.appdte.models;

/**
 *
 * @author esteban
 */
public class ImptoReten {

    public String getTipoimp() {
        return tipoimp;
    }

    public void setTipoimp(String tipoimp) {
        this.tipoimp = tipoimp;
    }

    public String getTasaimp() {
        return tasaimp;
    }

    public void setTasaimp(String tasaimp) {
        this.tasaimp = tasaimp;
    }

    public String getMontoimp() {
        return montoimp;
    }

    public void setMontoimp(String montoimp) {
        this.montoimp = montoimp;
    }
    
    private String tipoimp;
    private String tasaimp;
    private String montoimp;    
    
}
