/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.appdte.rest.resources;

import appdtews.documento.DocumentoModel;
import appdtews.empresa.EmpresaModel;
import appdtews.include.BlobDTE;
import appdtews.movimientos.MovimientoModel;
import com.appdte.json.PrintJSON;
import com.appdte.sii.utilidades.ConfigAppDTE;
import com.appdte.sii.utilidades.PrintBOLETA;
import com.appdte.sii.utilidades.PrintDTE;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;


@Path("/printDTE")
public class printDTE {
@POST
@Produces(MediaType.TEXT_PLAIN)
@Consumes(MediaType.APPLICATION_JSON)
public String getPDF(PrintJSON requestjson) throws ParserConfigurationException{

    try {
        String rut = requestjson.getRut();
        String folio=requestjson.getFolio();
        String codsii=requestjson.getCodsii();
        
        DocumentoModel objDocumentoModel = new DocumentoModel();
        
       int idtipodoc = objDocumentoModel.getId(codsii);
       
       MovimientoModel objMovimientoModel = new MovimientoModel();
       
       EmpresaModel objEmpresaModel = new EmpresaModel();
      int empresaid = objEmpresaModel.getIdEmpresafromRut(rut);
        
      int idmovmiento = objMovimientoModel.getIdMovimiento(idtipodoc,Integer.parseInt(folio),empresaid);
      
      BlobDTE objBlob = new BlobDTE();
     objBlob.getXMLDTE(idmovmiento);
      
        ConfigAppDTE objConfig = new ConfigAppDTE();
        
        String[] arrayrutemisor = rut.split("-");
        
        if("39".equals(codsii) || "41".equals(codsii)){
        PrintBOLETA objPrintBOLETA = new PrintBOLETA();
            objPrintBOLETA.printBOLETA(arrayrutemisor[0]+"F"+folio+"T"+codsii);
            
        }else{
            
        PrintDTE objPrintDTE = new PrintDTE();
        objPrintDTE.printDTE(arrayrutemisor[0]+"F"+folio+"T"+codsii);
        }
        String nombredte = objConfig.getPathpdf()+"ENVDTE"+arrayrutemisor[0]+"F"+folio+"T"+codsii+".pdf";
        String filePath = nombredte;
        byte[] bytesContent;
        bytesContent = Files.readAllBytes(Paths.get(filePath));
        return Base64.getEncoder().encodeToString(bytesContent);
    } catch (SAXException | IOException ex) {
        Logger.getLogger(printDTE.class.getName()).log(Level.SEVERE, null, ex);
    } catch (Exception ex) {
        Logger.getLogger(printDTE.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;

}  
    
}
