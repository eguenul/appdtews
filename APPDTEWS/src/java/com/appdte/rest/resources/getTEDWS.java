/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.appdte.rest.resources;

import appdtews.documento.DocumentoModel;
import appdtews.empresa.EmpresaModel;
import appdtews.include.BlobDTE;
import appdtews.movimientos.MovimientoModel;
import com.appdte.json.PrintJSON;
import com.appdte.sii.utilidades.getTED;
import java.io.IOException;
import java.sql.SQLException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 *
 * @author esteban
 */

@Path("/getTED")
public class getTEDWS {
@POST
@Produces(MediaType.TEXT_PLAIN)
@Consumes(MediaType.APPLICATION_JSON)
    public String getTED(PrintJSON requestjson) throws SQLException, ClassNotFoundException, ParserConfigurationException, SAXException, IOException, TransformerException{
       String rut = requestjson.getRut();
        String folio=requestjson.getFolio();
        String codsii=requestjson.getCodsii();
        
       DocumentoModel objDocumentoModel = new DocumentoModel();
        
       int idtipodoc = objDocumentoModel.getId(codsii);
       
       MovimientoModel objMovimientoModel = new MovimientoModel();
       
       EmpresaModel objEmpresaModel = new EmpresaModel();
       int empresaid = objEmpresaModel.getIdEmpresafromRut(rut);
        
        int idmovmiento = objMovimientoModel.getIdMovimiento(idtipodoc,Integer.parseInt(folio),empresaid);
      
      BlobDTE objBlob = new BlobDTE();
       objBlob.getXMLDTE(idmovmiento);
    
        
          getTED objget = new getTED();
          return  objget.getTED(rut, folio, codsii);
                
    }
    
}
    
    
    
    

