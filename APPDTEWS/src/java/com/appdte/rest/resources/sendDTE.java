/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.appdte.rest.resources;

import appdtews.documento.DocumentoModel;
import appdtews.empresa.EmpresaModel;
import appdtews.include.Funciones;
import appdtews.login.LoginModel;
import com.appboleta.sii.sendBOLETA;
import com.appdte.json.DteJson;
import com.appdte.json.EmisorJson;
import com.appdte.json.IdDteJson;
import com.appdte.json.TrackID;
import com.appdte.json.UsuarioJson;
import com.appdte.sii.utilidades.AppDTE;
import com.appdte.sii.utilidades.ConfigAppDTE;
import com.appdte.sii.utilidades.FuncionesCAF;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

@Path("/sendDTE")
public class sendDTE {
@POST
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public String sendDTE(DteJson objDTE) throws ParserConfigurationException, SAXException, IOException, Exception {
 
    final Gson gson = new Gson();
    final String stringDTE = gson.toJson(objDTE);

    UsuarioJson  objUsuario = objDTE.getUsuario();
    System.out.print(stringDTE);     
     /* DATOS DEL EMISOR EN JSON */
     
     
     LoginModel objLoginModel = new LoginModel();
       EmisorJson objemisor = objDTE.getEmisor();
      IdDteJson iddoc = objDTE.getIddoc();
     
    boolean flag_senddte = false; 
   
    flag_senddte = objLoginModel.authLogin(objUsuario.getLogin(), objUsuario.getPassword())==true;  
     
 Funciones objFunciones = new Funciones();
     
  if(flag_senddte==true){   
     objFunciones.loadCert(objUsuario.getLogin());
  }
  
  EmpresaModel objEmpresaModel = new EmpresaModel();
  
  int empresaid = objEmpresaModel.getIdEmpresafromRut(objemisor.getRutemisor());
  DocumentoModel objDocumentoModel = new DocumentoModel();
     
  int tipodocid = objDocumentoModel.getId(iddoc.getTipodte());
  objFunciones.loadCAF(empresaid, tipodocid, objUsuario.getLogin());
  
 
  System.out.print(objemisor.getRutemisor());  
    System.out.print(iddoc.getTipodte());
    
    
     FuncionesCAF objFuncionCAF = new FuncionesCAF();
    
    /* VALIDAMOS CAF */
    ConfigAppDTE objConfig = new ConfigAppDTE();
    
    flag_senddte = objFuncionCAF.validaCAF(objUsuario.getLogin(),objConfig.getPathcaf(), objemisor.getRutemisor(),Integer.parseInt(iddoc.getTipodte()), Integer.parseInt(iddoc.getNumdte())) != false;
    
    
    
    
    if(flag_senddte==true){
    
    if("39".equals(iddoc.getTipodte()) ||"41".equals(iddoc.getTipodte()) ){
        
                sendBOLETA objBOLETA = new sendBOLETA();
                String stringBOLETA = objBOLETA.sendBOLETA(stringDTE,objUsuario.getLogin(),objUsuario.getPassword(),objUsuario.getRut());

                Gson gson2 = new Gson(); 

                InputStream isjson = new ByteArrayInputStream(stringBOLETA.getBytes("UTF-8")); 
                BufferedReader br1 = new BufferedReader(new InputStreamReader(isjson));


               TrackID objtrack = gson2.fromJson(br1, TrackID.class);
               return gson2.toJson(objtrack);

        
       
        
    }else{
                AppDTE objAppDTE;
                objAppDTE = new AppDTE();
           
                String  respuesta = objAppDTE.sendDTE(stringDTE,objUsuario.getLogin(), objUsuario.getPassword(), objUsuario.getRut(), true);
                return respuesta;

        
      }
    }else{
        TrackID objTrackID = new TrackID();
        objTrackID.setRut_emisor(objemisor.getRutemisor());
        objTrackID.setRut_envia(objUsuario.getRut());
        objTrackID.setFecha_recepcion("-");
        objTrackID.setTrackid("0");
        objTrackID.setEstado("ERROR");
        Gson gson2 = new Gson(); 
        return gson2.toJson(objTrackID);

    } 
    
    
}   

}
