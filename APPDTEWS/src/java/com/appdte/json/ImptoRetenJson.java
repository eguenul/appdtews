/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.appdte.json;

/**
 *
 * @author esteban
 */
public class ImptoRetenJson {
 private String tipoimp;
 private String tasapimp;
 private String montoimp;

    public String getTipoimp() {
        return tipoimp;
    }

    public void setTipoimp(String tipoimp) {
        this.tipoimp = tipoimp;
    }

    public String getTasapimp() {
        return tasapimp;
    }

    public void setTasapimp(String tasapimp) {
        this.tasapimp = tasapimp;
    }

    public String getMontoimp() {
        return montoimp;
    }

    public void setMontoimp(String montoimp) {
        this.montoimp = montoimp;
    }
         
}
