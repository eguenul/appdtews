/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.appdte.json;

/**
 *
 * @author esteban
 */
public class PrintJSON {
   private String rut;
   private String folio;
   private String codsii;

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCodsii() {
        return codsii;
    }

    public void setCodsii(String codsii) {
        this.codsii = codsii;
    }
}
