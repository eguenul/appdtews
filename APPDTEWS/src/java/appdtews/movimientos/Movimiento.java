/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package appdtews.movimientos;

/**
 *
 * @author esteban
 */
public class Movimiento {
    private int idmovimiento;
    private String fecha;
    private int idtipodoc;
    private int numdoc;
    private int empresaid;
    private int trackid;
    private int totalbruto;
    private String rutemisor;
    private String rutreceptor;
    private String rutenvia;
 
 
 private String archivonom;
    public int getIdmovimiento() {
        return idmovimiento;
    }

    public void setIdmovimiento(int idmovimiento) {
        this.idmovimiento = idmovimiento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdtipodoc() {
        return idtipodoc;
    }

    public void setIdtipodoc(int idtipodoc) {
        this.idtipodoc = idtipodoc;
    }

    public int getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(int numdoc) {
        this.numdoc = numdoc;
    }

    public int getEmpresaid() {
        return empresaid;
    }

    public void setEmpresaid(int empresaid) {
        this.empresaid = empresaid;
    }

    public int getTrackid() {
        return trackid;
    }

    public void setTrackid(int trackid) {
        this.trackid = trackid;
    }

    public int getTotalbruto() {
        return totalbruto;
    }

    public void setTotalbruto(int totalbruto) {
        this.totalbruto = totalbruto;
    }

    public String getArchivonom() {
        return archivonom;
    }

    public void setArchivonom(String archivonom) {
        this.archivonom = archivonom;
    }

    public String getRutemisor() {
        return rutemisor;
    }

    public void setRutemisor(String rutemisor) {
        this.rutemisor = rutemisor;
    }

    public String getRutreceptor() {
        return rutreceptor;
    }

    public void setRutreceptor(String rutreceptor) {
        this.rutreceptor = rutreceptor;
    }

    public String getRutenvia() {
        return rutenvia;
    }

    public void setRutenvia(String rutenvia) {
        this.rutenvia = rutenvia;
    }
    

}
